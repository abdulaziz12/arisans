USE 08_dearavena_dbsurat

//Relasi di table_mail
ALTER TABLE mail ADD CONSTRAINT  FOREIGN KEY fk_mailtype(mail_typeid) REFERENCES mail_type(mail_typeid);
ALTER TABLE mail ADD CONSTRAINT  FOREIGN KEY fk_user(userid) REFERENCES USER(userid);

//Relasi di table_disposition
ALTER TABLE disposition ADD CONSTRAINT  FOREIGN KEY fk_disposition_mailid(mailid) REFERENCES mail(mailid);
ALTER TABLE disposition ADD CONSTRAINT  FOREIGN KEY fk_disposition_userid(userid) REFERENCES USER(userid);

//Insert_user
INSERT INTO `user` (`userid`, `username`, `password`, `fullname`, `level`) VALUES ('001', 'alfi', SHA1('alfi'), 'Alfi N', '1'), ('002', 'alfa', SHA1('alfa'), 'Alfa N', '2');
INSERT INTO `user` (`userid`, `username`, `password`, `fullname`, `level`) VALUES ('003', 'alfu', SHA1('alfu'), 'Alfu N', '3'), ('004', 'alfo', SHA1('alfo'), 'Alfo N', '4');
INSERT INTO `user` (`userid`, `username`, `password`, `fullname`, `level`) VALUES ('005', 'alfe', SHA1('alfe'), 'Alfe N', '5');

//insert_mailtype
INSERT INTO `mail_type` (`mail_typeid`, `type`) VALUES ('1', '1'), ('2', '2');
INSERT INTO `mail_type` (`mail_typeid`, `type`) VALUES ('3', '3'), ('4', '4');
INSERT INTO `mail_type` (`mail_typeid`, `type`) VALUES ('5', '5');

//insert_mail
INSERT INTO `mail` (`mailid`, `incoming_at`, `mail_code`, `mail_date`, `mail_from`, `mail_to`, `mail_subject`, `description`, `file_upload`, `mail_typeid`, `userid`) VALUES ('1', 'satu', '1', '2018-11-01', 'satu@gmail.com', 'one@gmail.com', 'onetry', 'satu itu one', 'dea.jpg', '1', '1'), ('2', 'dua', '2', '2018-11-02', 'dua@gmail.com', 'two@gmail.com', 'twotry', 'Dua itu two', 'dea2.jpg', '2', '2');
INSERT INTO `mail` (`mailid`, `incoming_at`, `mail_code`, `mail_date`, `mail_from`, `mail_to`, `mail_subject`, `description`, `file_upload`, `mail_typeid`, `userid`) VALUES ('3', 'tiga', '3', '2018-11-03', 'tiga@gmail.com', 'three@gmail.com', 'thereetry', 'tiga itu three', 'dea3.jpg', '3', '3'), ('4', 'empat', '4', '2018-11-04', 'empat@gmail.com', 'four@gmail.com', 'fourtry', 'empat itu four', 'dea4.jpg', '4', '4')
INSERT INTO `mail` (`mailid`, `incoming_at`, `mail_code`, `mail_date`, `mail_from`, `mail_to`, `mail_subject`, `description`, `file_upload`, `mail_typeid`, `userid`) VALUES ('5', 'lima', '5', '2018-11-05', 'lima@gmail.com', 'five@gmail.com', 'fivetry', 'lima itu five', 'dea5.jpg', '5', '5')

//insert_disposition
INSERT INTO `disposition` (`id`, `disposition_at`, `reply_at`, `description`, `notification`, `mailid`, `userid`, `status_`, `dispositionid`) VALUES ('1', 'dispositionone', 'one', 'one itu satu', 'satuuuuuuuu', '1', '1', 'succes', '1'), ('2', 'dispositiontwo', 'two', 'two itu dua', 'duaaaaaaa', '2', '2', 'succes two', '2');
INSERT INTO `disposition` (`id`, `disposition_at`, `reply_at`, `description`, `notification`, `mailid`, `userid`, `status_`, `dispositionid`) VALUES ('3', 'dispositionthree', 'three', 'three itu tiga', 'tigaaaaa', '3', '3', 'succes ketuga', '3'), ('4', 'dispositionfour', 'four', 'four itu empat', 'empattttt', '4', '4', 'succes four', '4');
INSERT INTO `disposition` (`id`, `disposition_at`, `reply_at`, `description`, `notification`, `mailid`, `userid`, `status_`, `dispositionid`) VALUES ('5', 'dispositionfive', 'five', 'five itu lima', 'limaaaa', '5', '5', 'succes kelima', '5');

//DaftarSuratOktober2018 (NO 3)
CREATE VIEW SuratDi2018 AS
SELECT mail.`mailid`, mail.`incoming_at`,mail.`mail_date`,mail.`mail_from`, mail.`mail_subject`, mail.`description`,mail_type.`type`
FROM mail JOIN mail_type ON mail.`mail_typeid` = mail_type.`mail_typeid` ORDER BY mail.`mail_date`; 

//DaftarSuratDispositionOne
CREATE VIEW SuratDisposisiUnitOne AS
SELECT mail.`mailid`, mail.`incoming_at`,mail.`mail_code`,mail.`mail_from`, mail.`mail_subject`, mail.`description`,mail_type.`type`, disposition.`disposition_at`
FROM mail , mail_type, disposition WHERE mail.`mail_typeid` = mail_type.`mail_typeid` AND disposition.`mailid` = mail.`mailid` AND disposition.`disposition_at` = 'dispositionone';

//NO5 Nampilin Diposition tanggal dan count tiap tanggal
SELECT disposition.`disposition_at` , mail.`mail_date` , COUNT(user.`userid`) 
FROM disposition JOIN mail ON disposition.`mailid` = mail.`mailid` JOIN USER ON disposition.`userid` = user.`userid` GROUP BY user.`userid`
