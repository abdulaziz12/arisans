# ArisanS
Aplikasi Arisan Simple berbasis Website

untuk database, aplikas ini sudah menggunakan migration, jadi tinggal di migrate saja

## 1. Menu Arisan (Menu Utama)
Page ini menampilkan data-data arisan yang dibutuhkan
![adddata](ss/menu.PNG)

## 2. Add Data 
Pada bagian ini digunakan untuk mengisi data-data arisan, dimana data-data tersebut akan tersimpan didatabase
![adddata](ss/tambah.PNG)

## 3. Delete Data
Ketika anda meng-klik button delete, akan ada pemberitahuan seperti ini terlebih dahulu agar lebih nyaman
![adddata](ss/delete.PNG)

## 4. Edit Data
Page ini akan mencul ketika anda menekan button "Edit", yang nantinya akan mengubah data didabase
![adddata](ss/edit.PNG)

## 5. Search
Fitur ini digunakan untuk mempermudah anda dalam mencari data
![adddata](ss/search.PNG)

## 6. Bayar
Fitur ini digunakan untuk mengubah status bayar yang mana jika diklik akan otomatis status anda menjad "Bayar"
![adddata](ss/bayar.PNG)


Sekian penjelasan dari saya. Semoga bermafaat