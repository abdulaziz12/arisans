<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTArisan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('t_arisan', function (Blueprint $table) {
            $table->Increments('id');
            $table->String('nm_anggota', 50);
            $table->String('alamat', 100);
            $table->enum('status_bayar',['Belum Bayar','Sudah Bayar']);
            $table->enum('status_menang',['Belum Menang','Sudah Menang']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_arisan');
    }
}
