<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Arisan;
use DataTables;

class ArisanController extends Controller
{

  public function index(){
    return view('index');
  }

  public function json(){

      $arisan = DB::select("SELECT * FROM t_arisan");
      return Datatables::of($arisan)
      ->addColumn('action', function($arisan){
      return '<a href="/'.$arisan->id.'/show_arisan" class="btn btn-sm btn-warning edit" id=""><i class="glyphicon glyphicon-edit"></i>&nbsp Edit &nbsp&nbsp&nbsp</a>
      <a href="/delete_arisan/'.$arisan->id.'" class="btn btn-sm btn-danger delete" id=""><i class="glyphicon glyphicon-remove"></i> Hapus</a>
      <a href="/update_bayar/'.$arisan->id.'" class="btn btn-sm btn-success edit" id="">Bayar</a>';


      })

      ->make(true);
  }

  public function deleteArisan($id){
               DB::delete('delete from t_arisan where id = ?',[$id]);
               $after_save = [
                         'alert' => 'success',
                         'icon' => 'check',
                         'title' => 'Berhasil ! ',
                         'text-1' => 'Data Arisan ',
                         'text-2' => 'Berhasil dihapus.'
                     ];
               return redirect('/')->with('after_save', $after_save);

  }


  public function tambah_arisan(){

     return view('formtambah');
  }

  public function storeArisan(Request $request){

    $validate = \Validator::make($request->all(), [
              'nm_anggota' => 'required',
              'alamat'=> 'required'

            ]);


      $data = new Arisan();
         $data->nm_anggota = $request->nm_anggota;
         $data->alamat = $request->alamat;
         $data->status_bayar = 'Belum Bayar';
         $data->status_menang = 'Belum Menang';
         $data->save();

         return redirect('/');
  }

  public function showArisan($id)
  {
    $arisan = Arisan::where('id', $id)->first();
    return view('/foredit', compact('arisan'));

  }

  public function updateArisan(Request $request){
      $id = $request->input('id');
      $arisan= Arisan::where('id', $id);
      $arisan->update([
        'nm_anggota' => $request->nm_anggota,
        'alamat' => $request->alamat
      ]);
      $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data ',
                'text-2' => 'Telah diubah.'
            ];
      return redirect('/')->with('after_save', $after_save);

  }

  public function updateBayar(Request $request , $id){

      $arisan= Arisan::where('id', $id);
      $arisan->update([
        'status_bayar' => 'Sudah Bayar',
      ]);
      $after_save = [
                'alert' => 'success',
                'icon' => 'check',
                'title' => 'Berhasil ! ',
                'text-1' => 'Data ',
                'text-2' => 'Telah diubah.'
            ];
      return redirect('/')->with('after_save', $after_save);

  }


}
