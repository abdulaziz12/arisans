<?php

namespace App\Exports;

use App\Model\frontend\Pendaftaran;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;

class PendaftaranExport implements FromQuery, WithHeadings, ShouldAutoSize
{
  use Exportable;

    public function forDate(string $date, $datee)
   {
       $this->date = $date;
       $this->datee = $datee;

       return $this;
   }

  public function query()
  {
    return Pendaftaran::query()->whereBetween('waktu_pendaftar',[$this->date , $this->datee]);
  }
  public function headings(): array
  {
    return [
         'Id Pendaftaran',
         'Nama Perusahaan',
         'Keterangan Kegiatan',
         'Lokasi Perusahaan',
         'Lokasi Kegiatan',
         'Dokumen Dimiliki',
         'Luas Lokasi',
         'Rencana Pembuatan',
         'Waktu Pendaftaran',
         'Pembayaran',
         'Tanggal Mulai Pembuatan',
         'Tanggal Selesai Pembuatan',
         'Status',
         'Dokumen Hasil',
         'Id Perusahaan'

     ];
  }
}
