<?php

namespace App\Exports;

use App\Model\frontend\Perusahaan;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;

class PerusahaanExport implements FromQuery, WithHeadings, ShouldAutoSize
{
  use Exportable;

    public function forDate(string $date, $datee)
   {
       $this->date = $date;
       $this->datee = $datee;

       return $this;
   }

  public function query()
  {
    return Perusahaan::query()->whereBetween('ket_waktu',[$this->date , $this->datee]);
  }
  public function headings(): array
  {
    return [
         'Id Perusahaan',
         'Nama Perusahaan',
         'Pemimpin',
         'Tanggal Berdiri',
         'Lokasi Perusahaan',
         'No Telepon',
         'Keterangan',
         'Keterangan Waktu'

     ];
  }
}
