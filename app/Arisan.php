<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arisan extends Model
{
  protected $table='t_arisan';

  protected $fillable = ['id','nm_anggota','alamat','status_bayar','status_menang'];
}
