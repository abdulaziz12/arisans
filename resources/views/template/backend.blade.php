<!doctype html>
<html lang="en">

<head>
	<title>Arisan</title>

	@stack('css')
<style>
/*-----------
preloader
------------*/
.preloader {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #2B333E ;
	filter: progid:DXImageTransform.Microsoft.grad ient( startColorstr='#75cbe7', endColorstr='#138fc2', GradientType=0);
	z-index: 99999999999;
}

.sk-folding-cube {
	margin: 20px auto;
	width: 40px;
	height: 40px;
	position: absolute;
	top: 50%;
	left: 50%;
	margin-top: -20px;
	margin-left: -20px;
	-webkit-transform: rotateZ(45deg);
	transform: rotateZ(45deg);
}

.sk-folding-cube .sk-cube {
	float: left;
	width: 50%;
	height: 50%;
	position: relative;
	-webkit-transform: scale(1.1);
	transform: scale(1.1);
}

.sk-folding-cube .sk-cube:before {
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: #ffffff;
	-webkit-animation: sk-foldCubeAngle 2.4s infinite linear both;
	animation: sk-foldCubeAngle 2.4s infinite linear both;
	-webkit-transform-origin: 100% 100%;
	transform-origin: 100% 100%;
}

.sk-folding-cube .sk-cube2 {
	-webkit-transform: scale(1.1) rotateZ(90deg);
	transform: scale(1.1) rotateZ(90deg);
}

.sk-folding-cube .sk-cube3 {
	-webkit-transform: scale(1.1) rotateZ(180deg);
	transform: scale(1.1) rotateZ(180deg);
}

.sk-folding-cube .sk-cube4 {
	-webkit-transform: scale(1.1) rotateZ(270deg);
	transform: scale(1.1) rotateZ(270deg);
}

.sk-folding-cube .sk-cube2:before {
	-webkit-animation-delay: 0.3s;
	animation-delay: 0.3s;
}

.sk-folding-cube .sk-cube3:before {
	-webkit-animation-delay: 0.6s;
	animation-delay: 0.6s;
}

.sk-folding-cube .sk-cube4:before {
	-webkit-animation-delay: 0.9s;
	animation-delay: 0.9s;
}

@-webkit-keyframes sk-foldCubeAngle {
	0%,
	10% {
			-webkit-transform: perspective(140px) rotateX(-180deg);
			transform: perspective(140px) rotateX(-180deg);
			opacity: 0;
	}
	25%,
	75% {
			-webkit-transform: perspective(140px) rotateX(0deg);
			transform: perspective(140px) rotateX(0deg);
			opacity: 1;
	}
	90%,
	100% {
			-webkit-transform: perspective(140px) rotateY(180deg);
			transform: perspective(140px) rotateY(180deg);
			opacity: 0;
	}
}

@keyframes sk-foldCubeAngle {
	0%,
	10% {
			-webkit-transform: perspective(140px) rotateX(-180deg);
			transform: perspective(140px) rotateX(-180deg);
			opacity: 0;
	}
	25%,
	75% {
			-webkit-transform: perspective(140px) rotateX(0deg);
			transform: perspective(140px) rotateX(0deg);
			opacity: 1;
	}
	90%,
	100% {
			-webkit-transform: perspective(140px) rotateY(180deg);
			transform: perspective(140px) rotateY(180deg);
			opacity: 0;
	}
}

</style>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('assets')}}/backend/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{asset('assets')}}/backend/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('assets')}}/backend/vendor/linearicons/style.css">
	<link rel="stylesheet" href="{{asset('assets')}}/backend/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="{{asset('assets')}}/backend/css/jquery.dataTables.min.css">
	<link href="{{asset('assets')}}/backend/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('assets')}}/backend/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{asset('assets')}}/backend/css/demo2.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" href="{{asset('assets')}}/backend/images/apple-touch-icon.png">
	<link rel="shortcut icon" type="{{asset('assets')}}/backend/image/ico" href="{{asset('assets')}}/backend/images/favicon.ico" />
</head>


<body>
	<div class="preloader">
				 <div class="sk-folding-cube">
						 <div class="sk-cube1 sk-cube"></div>
						 <div class="sk-cube2 sk-cube"></div>
						 <div class="sk-cube4 sk-cube"></div>
						 <div class="sk-cube3 sk-cube"></div>
				 </div>
		 </div>
	<div id="wrapper">
		<!-- NAVBAR -->

		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div class="navbar-form navbar-left">
					<span style="font-size:23px; color:grey; font-family:Calibri;">@yield('judul')</span>
				</div>


				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<div class="pull-right">
								{{-- part alert --}}
											 @if (Session::has('after_savee'))
																	 <div class="alert alert-dismissible alert-{{ Session::get('after_savee.alert') }}">
																		 <i class="pe-7s-{{ Session::get('after_savee.icon') }}" style="font-size:30px; position:relative; top:8px;"></i>
																		 <button type="button" class="close" data-dismiss="alert">×</button>
																		 <strong>{{ Session::get('after_savee.title') }}</strong>
																		 <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_savee.text-1') }}</a> {{ Session::get('after_savee.text-2') }}
																	 </div>
												@endif
								 {{-- end part alert --}}
						</div>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="{{url('/index')}}" class="active"><i class="lnr lnr-home"></i> <span style="font-family:Calibri; font-size:16px;">Arisan</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		@yield('content')
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <footer class="footer">
              <div class="container-fluid">
                  <p class="copyright pull-right">
                      &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">ArisanS
                  </p>
              </div>
          </footer>
	</div>
	<!-- Javascript -->
	<script src="{{asset('assets')}}/backend/vendor/jquery/jquery.min.js"></script>
	<script src="{{asset('assets')}}/backend/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="{{asset('assets')}}/backend/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="{{asset('assets')}}/backend/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="{{asset('assets')}}/backend/js/klorofil-common.js"></script>
	<script src="{{asset('assets')}}/backend/js/jquery.dataTables.min.js" ></script>
	@stack('scripts')
</body>

</html>
