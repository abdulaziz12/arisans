@extends('template/backend')

@section('judul')
Edit Arisan
@endsection

<!-- Created by Aziz-->

@section('content')
<div class="main">
<div class="container-fluid">
   <div class="row" style="padding:20px;"><br>
     <div class="panel">
       <div class="panel-heading">
         <h3> Data Arisan</h3>
       </div>
        <div class="panel-body">
        <form action="{{ URL('/update_arisan')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="number" hidden value="{{$arisan->id}}" name="id">
          <div class="modal-body">
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Anggota</label>
                        <input type="text" name="nm_anggota" class="form-control" value="{{$arisan->nm_anggota}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat" class="form-control" value="{{$arisan->alamat}}" required>
                    </div>
                </div>
              </div>
         </div>

         <div class="modal-footer">
             <a href="/"><i class="glyphicon glyphicon-arrow-left"> </i> Back</a>
             <button type="submit" class="btn btn-primary btn-save btn-fill"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
         </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">

$(window).on("load", function () {
  $('.preloader').fadeOut(4000);
});

</script>
@endpush
