@extends('template/backend')

@section('judul')
Data Arisan
@endsection

@section('content')
<div class="main">
<div class="container-fluid">
   <div class="row" style="padding:20px;"><br>
     <div class="panel">
       <div class="panel-heading">
         <h3> Data Arisan</h3>
       </div>
        <div class="panel-body">
          <a class="btn btn-sm btn-primary" href="#" data-toggle="modal" data-target="#addForm" style="margin-left:10px;">Tambah
          <i class="glyphicon glyphicon-plus"></i></a>

        <div class="modal fade" tabindex="-1" id="addForm" role="dialog" data-backdrop="false">
         <div class="modal-dialog" role="document" >
             <div class="modal-content">
                 <!-- Modal Header -->
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true"> &times;</span>
                   </button>
                   <h3 class="modal-title"><b>Tambah Barang</b></h3>
                 </div>

                 <form action="{{ URL('/validasi')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                   {{ csrf_field() }}
                   <div class="modal-body">
                      <div class="row">
                         <div class="col-md-6">
                             <div class="form-group">
                                 <label>Nama Anggota</label>
                                 <input type="text" name="nm_anggota" class="form-control" placeholder="Masukkan Nama" required>
                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="form-group">
                                 <label>Alamat</label>
                                 <input type="text" name="alamat" class="form-control" placeholder="Masukkan Alamat" required>
                             </div>
                         </div>
                       </div>
                  </div>

                  <div class="modal-footer">
                      <button type="reset" class="btn btn-raised btn-warning btn-fill"><i class="glyphicon glyphicon-refresh"> </i> Reset</button>
                      <button type="submit" class="btn btn-primary btn-save btn-fill"><i class="glyphicon glyphicon-floppy-disk"></i> Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="table-responsive">
            <table  class="table table-striped table-bordered table-hover " id="t_arisan"  >
              <thead>
                <tr>
                  <td><b>No</b></td>
                  <td><b>Nama Anggota</b></td>
                  <td><b>Alamat</b></td>
                  <td><b>Status Bayar</b></td>
                  <td><b>Status Menang</b></td>
                  <td><b>Action</b></td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

  $(window).on("load", function () {
    $('.preloader').fadeOut(4000);
  });


$(function() {
  var t = $('#t_arisan').DataTable({
        pagingType:"full_numbers",
        processing: true,
        "language": {
          processing: "<i class='fa fa-spinner fa-spin fa-3x fa-fw style='color:#0A6BAE;'></i><span class='sr-only'>Loading...</span> "},
        serverSide: true,
        ajax:'{{ route("arisan.json") }}',
        columns: [
            { data: null, orderable: false},
            { data: 'nm_anggota', name: 'nm_anggota' },
            { data: 'alamat', name: 'alamat' },
            { data: 'status_bayar', name: 'status_bayar' },
            { data: 'status_menang', name: 'status_menang' },

            { data: 'action', orderable:false, searchable:false }
          ],
          dom: 'B<"toolbar">ifrtlp',
          "rowCallback": function (nRow, aData, iDisplayIndex) {
           var oSettings = this.fnSettings ();
           $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
           return nRow;
         }
 } );
 $(document).on('click', '.delete', function(){
       if(confirm("Apakah anda ingin Menghapus data ini?"))
       {
        alert('Data Berhasil di Hapus.'); window.location.href='/dataArisan';
       }
       else
       {
           return false;
       }
   });
});

</script>
@endpush
